from itertools import count

import networkx as nx


class Map:
    def __init__(self):
        self.tree = nx.DiGraph()
        self._node_id_generator = count()

    def generate_node_id(self):
        return next(self._node_id_generator)
