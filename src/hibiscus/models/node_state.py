from enum import Enum


class NodeState(Enum):
    NEUTRAL = ""
    APPROVED = "*"
    REJECTED = "/"
    PINNED = "^"
