from enum import Enum


class NodeData(Enum):  # TODO turn into a dataclass
    TYPE = "type"
    STATE = "state"
    CONTENT = "content"
