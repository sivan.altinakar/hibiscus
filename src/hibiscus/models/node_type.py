from enum import Enum


class NodeType(Enum):
    QUESTION = "?"
    IDEA = "!"
    PRO = "+"
    CON = "-"
    NOTE = "="
    MAP = "&"
    LINK = "@"
