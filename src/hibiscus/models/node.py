from __future__ import annotations

import click

from .map import Map
from .node_type import NodeType
from .node_state import NodeState
from .node_data import NodeData


# TODO move node types inside Node
# TODO alias NodeContent type to str
# TODO alias NodeId to str
# TODO rename parser to reader
# MAYBE rename Node to HNode and Map to HMap


class Node:
    def __init__(self, map: Map, id: int):
        self.map: Map = map
        self.id: int = id

    @property
    def data(self) -> dict:
        return self.map.tree.nodes[self.id]

    @property
    def type(self) -> NodeType:
        return self.data[NodeData.TYPE.value]

    @property
    def state(self) -> NodeState:
        return self.data[NodeData.STATE.value]

    @property
    def content(self) -> str:
        return self.data[NodeData.CONTENT.value]

    @property
    def subject(self) -> Node:
        result = Node(self.map, next(self.map.tree.successors(self.id)))

        # click.echo(f"{self.id} has successor {result.id}")  # DEBUG

        return result

    def move_toward_root(self, steps: int) -> Node:
        result = self

        # click.echo(result.data)  # DEBUG

        for _ in range(steps):
            # result = next(self.map.tree.successors(result.id))
            result = result.subject

            # click.echo(result.data)  # DEBUG

        return result

    @property
    def observations(
        self,
    ) -> list:  # TODO choose format to return, probably list of nodes
        return self.map.tree.predecessors(self.id)

    # TODO depth property

    def add_new_observation(
        self, type: NodeType, content: str, state: NodeState = NodeState.NEUTRAL
    ) -> Node:
        new_node = self._create_node(self.map, type, content, state)
        self.map.tree.add_edge(new_node.id, self.id)
        return new_node

    @classmethod
    def create_root_subject(
        cls, map, type: NodeType, content: str, state: NodeState = NodeState.NEUTRAL
    ) -> Node:
        return cls._create_node(map, type, content, state)

    @staticmethod
    def _create_node(map: Map, type: NodeType, content: str, state: NodeState) -> Node:
        new_id = map.generate_node_id()
        map.tree.add_node(
            new_id,
            **{  # TODO rework
                NodeData.TYPE.value: type.name,
                NodeData.STATE.value: state.name,
                NodeData.CONTENT.value: content,
            },
        )
        return Node(map, new_id)
