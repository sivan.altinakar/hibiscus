from pathlib import Path

import click

from .readers.hibis import read_hibis
from .writers.html import write_html


@click.command()
@click.argument(
    "input", type=click.Path(exists=True, readable=True, dir_okay=False, path_type=Path)
)
@click.argument(
    "output",
    type=click.Path(writable=True, dir_okay=False, path_type=Path),
)
def main(input: Path, output: Path):
    map = read_hibis(input)
    write_html(output, map)


main()
