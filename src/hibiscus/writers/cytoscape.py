import json
from pathlib import Path

import click
import networkx as nx

from hibiscus.models.map import Map


def write_cytoscape(output: Path, map: Map):
    cyto = convert_to_cytoscape(map)
    # click.echo(cyto)
    # click.echo(json.dumps(cyto, indent=4))
    output.write_text(json.dumps(cyto, indent=4))


def convert_to_cytoscape(map):
    return nx.cytoscape_data(map.tree)
