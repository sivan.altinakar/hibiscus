from pathlib import Path
from typing import Final

from jinja2 import Template

from ..models.map import Map
from .cytoscape import convert_to_cytoscape
from . import html_assets
from .html_assets.styles.compendium import STYLE
from .html_assets.layouts.klay import JS_PACKAGES, LAYOUT

from importlib.resources import files

TEMPLATE: Final[Template] = Template((files(html_assets) / "template.html").read_text())


def write_html(output: Path, map: Map):
    cyto = convert_to_cytoscape(map)
    text = TEMPLATE.render(
        js_packages=JS_PACKAGES, layout=LAYOUT, style=STYLE, elements=cyto["elements"]
    )
    # click.echo(text)
    output.write_text(text)


# def _get_template():
#     return Environment(
#         loader=PackageLoader("hibiscus.writers.html"), autoescape=select_autoescape()
#     ).get_template(TEMPLATE_NAME)
