from typing import Final


STYLE: Final = [
    {
        "selector": "node",
        "style": {
            "label": "data(content)",
            "text-wrap": "wrap",
            "text-max-width": 200,
            "text-justification": "left",
            # "text-halign": "right",
            # "text-valign": "center",
            # "text-margin-x": 5,
            "text-halign": "center",
            "text-valign": "bottom",
            "text-margin-y": 5,
            "font-family": "Atkinson Hyperlegible, helvetica",
            "shape": "circle",
            "height": 30,
            # "width": "label",
            "width": 30,
            # "padding": 6,
            "background-color": "lightgray",  # default
            "background-fit": "contain",
            # "color": "white",
            "text-background-color": "white",
            "text-background-opacity": 0.8,
            "text-background-shape": "round-rectangle",
            "text-background-padding": 5,
        },
    },
    # color palette 18 from https://venngage.com/blog/blue-color-palettes/
    # #ff595e # #ffca3a # #8ac926 # #1982c4 # #6a4c93
    {
        "selector": 'node[type="QUESTION"]',
        "style": {
            "background-color": "#21A9FF",  # blue #1982c4 or purlple #6a4c93
            # attribution: <a  href="https://icons8.com/icon/98973/question">Question</a> icon by <a href="https://icons8.com">Icons8</a>
            "background-image": "https://img.icons8.com/ios-glyphs/90/question-mark.png",
        },
    },
    {
        "selector": 'node[type="IDEA"]',
        "style": {
            "background-color": "#ffca3a",
            # attribution: <img width="100" height="100" src="https://img.icons8.com/ios/100/light-on--v1.png" alt="light-on--v1"/>
            "background-image": "https://img.icons8.com/ios/100/light-on--v1.png",
        },
    },
    {
        "selector": 'node[type="PRO"]',
        "style": {
            "background-color": "#8ac926",
            # attribution: <a  href="https://icons8.com/icon/80109/xbox-cross">Xbox Cross</a> icon by <a href="https://icons8.com">Icons8</a>
            "background-image": "https://img.icons8.com/ios-glyphs/90/xbox-cross.png",
        },
    },
    {
        "selector": 'node[type="CON"]',
        "style": {
            "background-color": "#ff595e",
            # attribution: <a  href="https://icons8.com/icon/79029/dash">Dash</a> icon by <a href="https://icons8.com">Icons8</a>
            "background-image": "https://img.icons8.com/ios-glyphs/90/minus-math.png",
        },
    },
    # {
    #     "selector": 'node[type="NOTE"]',
    #     "style": {
    #         # 'background-color': '',
    #         # attribution:
    #         "background-image": ,
    #     }
    # },
    {
        "selector": "edge",
        "style": {
            "width": 4,
            "target-arrow-shape": "triangle",
            "line-color": "darkgray",
            "target-arrow-color": "darkgray",
            "curve-style": "bezier",
            # "curve-style": "taxi",
            "taxi-direction": "rightward",
            "source-endpoint": "outside-to-node-or-label",
            "target-endpoint": "outside-to-node-or-label",
            "source-distance-from-node": 10,
            "target-distance-from-node": 10,
        },
    },
]
