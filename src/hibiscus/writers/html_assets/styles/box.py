from typing import Final


STYLE: Final = [
    {
        "selector": "node",
        "style": {
            "label": "data(content)",
            "text-wrap": "wrap",
            "text-max-width": 200,
            "text-justification": "left",
            "text-halign": "right",
            "text-valign": "center",
            "font-family": "Atkinson Hyperlegible, helvetica",
            "shape": "round-rectangle",
            "height": "label",
            # "width": "label",
            "width": 200,
            "padding": 6,
            "text-margin-x": -203,  # -(width + padding/2)
            "background-color": "lightgray",
            # "color": "white"
        },
    },
    # color palette 18 from https://venngage.com/blog/blue-color-palettes/
    # #ff595e # #ffca3a # #8ac926 # #1982c4 # #6a4c93
    {
        "selector": 'node[type="QUESTION"]',
        "style": {"background-color": "#1982c4"},  # blue #1982c4 or purlple #6a4c93
    },
    {"selector": 'node[type="IDEA"]', "style": {"background-color": "#ffca3a"}},
    {"selector": 'node[type="PRO"]', "style": {"background-color": "#8ac926"}},
    {"selector": 'node[type="CON"]', "style": {"background-color": "#ff595e"}},
    {
        "selector": "edge",
        "style": {
            "width": 4,
            "target-arrow-shape": "triangle",
            "line-color": "darkgray",
            "target-arrow-color": "darkgray",
            # 'curve-style': 'bezier'
            "curve-style": "taxi",
            "taxi-direction": "rightward",
        },
    },
]
