from typing import Final, Union
import re
from re import Pattern
from pathlib import Path

import click

from ..models.map import Map
from ..models.node import Node
from ..models.node_type import NodeType
from ..models.node_state import NodeState

NODE_LINE_PATTERN: Final[Pattern] = re.compile(
    r"^(?P<indent>[ ]*)(?P<type>\S)(?P<state>\S?) (?P<content>.*)$"
)


# TODO turn into HibisParser class
# TODO raise custom exceptions
def read_hibis(input: Union[str, Path]) -> Map:
    text = input.read_text() if isinstance(input, Path) else input

    # TOOO assert no tab

    map = Map()
    previous_node = None
    previous_node_depth = None  # TODO use internal depth property
    current_node = None
    current_node_depth = None
    indent_unit = None

    for a_line_number, a_line in enumerate(text.split("\n")):
        if not a_line.strip():
            continue
        parsed_line = re.match(NODE_LINE_PATTERN, a_line)
        assert (
            parsed_line
        ), f"ERROR line {a_line_number}: cannot parse '{a_line.strip()}'"

        # click.echo(parsed_line.groupdict())  # DEBUG

        indent_length = len(parsed_line["indent"])
        node_data = {
            "type": NodeType(parsed_line.group("type")),
            "state": NodeState(parsed_line.group("state")),
            "content": parsed_line.group("content"),
        }

        if previous_node is None:
            assert (
                indent_length == 0
            ), f"ERROR line {a_line_number}: first node cannot be indented"
            current_node_depth = 0
            current_node = Node.create_root_subject(map, **node_data)
        elif indent_unit is None:
            assert (
                indent_length > 0
            ), f"ERROR line {a_line_number}: second node must be indented"
            current_node_depth = 1
            indent_unit = indent_length
            current_node = previous_node.add_new_observation(**node_data)
        else:
            assert (indent_length == 0) or (
                indent_length % indent_unit == 0
            ), f"ERROR line {a_line_number}: indentation ({indent_length}) not a multiple of indent unit ({indent_unit})"
            current_node_depth = indent_length // indent_unit if indent_length else 0
            assert (
                current_node_depth <= previous_node_depth + 1
            ), f"ERROR line {a_line_number}: indentation depth ({current_node_depth}) too large relative to previous node ({previous_node_depth})"
            subject = previous_node.move_toward_root(
                steps=previous_node_depth + 1 - current_node_depth
            )
            current_node = subject.add_new_observation(**node_data)

        # click.echo(current_node_depth)  # DEBUG

        previous_node = current_node
        previous_node_depth = current_node_depth

    return map
